json.extract! brain, :id, :flavor, :string, :created_at, :updated_at
json.url brain_url(brain, format: :json)
