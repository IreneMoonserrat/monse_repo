
class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "is not an email")
    end
  end
end
class Zombie < ApplicationRecord
	has_many :brains
	mount_uploader :avatar, FotoUploader 
validates :bio, length: { maximum: 100 }
validates :name, presence: true
validates :age, numericality: {only_integer: true, message: "Sólo numeros enteros"}
validates :email, presence: true, email: true
end
